# fourWins

Simple fourwins sdl project.

![Alt text](screenshot.png "FourWins")

## Details
<p>Makes use of sdl_mixer and self made sounds, currently none in repo due to the use placeholder sounds which are not mine. You could simply include your own (win.wav, set.wav, select.wav) to the root dir if you wanted</p>
<p>Makes use of sdl_ttf, currently the LiberationMono-Regular.ttf font is expected at the root dir. Until Im sure that I can upload it here I'll leave it like that.</p>

I really didn't need c++ class concept to do this, but I wanted to get used to it for further projects.

## Build
Install: SDL2, SDL_mixer, SDL_ttf.
If you are on linux you just install these 3 by unpacking each archive and run "./configure, make, make install" one after another for each one.
<p>Then run buildFourWins.sh</p>

## Run
<p>Execute the output "fourWins" from your build</p>