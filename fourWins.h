#pragma once

#include <SDL2/SDL.h>
#include <SDL_mixer.h>
#include <SDL_ttf.h>
#include <array>
#include <iostream>

class cGame {
private:
  // Make sure the right sdl version is being used
  SDL_version vcompiled;
  SDL_version vlinked;

  // Screen dimension constants
  const int SCREEN_WIDTH = 1000;
  const int SCREEN_HEIGHT = 1000;

  // Frames per second, not used yet !
  const int_fast8_t fps = 30;

  // Grid Size 7x6
  const int rows = 6;
  const int columns = 7;

  // OuterGrid
  const SDL_Rect outRect = {10, 10, SCREEN_WIDTH - 20, SCREEN_HEIGHT - 20};

  // InnerGrid 7x6 = 42
  std::array<SDL_Rect, 42> innerGrid;

  // Pointers Visuals
  SDL_Window *pWindow = nullptr;
  SDL_Renderer *pRenderer = nullptr;

  // Pointers Sound
  Mix_Chunk *pSoundSelect = nullptr;
  Mix_Chunk *pSoundSetToken = nullptr;
  Mix_Chunk *pSoundWin = nullptr;

  // Pointers Text
  TTF_Font *pfont = nullptr;
  SDL_Surface *pSurfaceText = nullptr;
  SDL_Texture *pTexture = nullptr;

  // Square Dest Text
  SDL_Rect rText = {100, 890, 800, 30};
  const std::string sText = "Use <- and -> arrow keys to navigate, space to set your token";

  // The current Selection X
  int iSelection = 3;
  // Temp to check if a new field was selected
  int iTempSelection = 3;
  // Schedule count, this holds the number of an animations target square
  int iScheduler = 0;
  int iAnimateSet = 0;
  int iAnimateStart = 0;

  // Colors
  const SDL_Colour cGrid = {0xFF, 0x00, 0x00, 0xFF};
  // Electric Purple
  const SDL_Colour cSelection = {191, 0, 230, 255};
  // Screaming Green
  const SDL_Colour cGreen = {102, 255, 102, 255};
  // Rich eletric Blue
  const SDL_Colour cBlue = {0, 136, 204, 255};

  enum eToken { Green,
                Blue };

  // Token Colour
  struct TokenColour {
    enum eToken TokenColourName;
    char TokenString[10];
    SDL_Colour cToken;
  };

  // 2 Possible Tokens
  const TokenColour BlueToken = {Blue, "Blue", cBlue};
  const TokenColour GreenToken = {Green, "Green", cGreen};

  // A TokenCross
  struct TokenCross {
    SDL_Point Cross[4];
    TokenColour sTokenColour;
    bool bSet = false;
  };
  std::array<TokenCross, 42> TokenMap;
  TokenCross TokenSelection;

  // The offset a X has to the grid, only used by DrawX()
  const int_fast8_t iOffset = 10;
  // Increases 2 each frame, used to make winning tokens spinning
  int iRotationValue = 0;
  
  // Round Counter, Green Token starts
  int RoundCount = 1;

  // Animiation to process ?
  bool bAnimateSet = false;
  bool bAnimateWin = false;
  // Flag to end game
  bool bEndGame = false;

  // To store the location of the 4 winning tokens
  std::array<int, 4> winfields = {0, 0, 0, 0};

public:
  // Init Game
  cGame();
  // Init inner Grid
  void InitinnerGrid();
  // Draw the outer and inner game grid
  void DrawGrid();
  // Returns the current field the mouse is in
  int GetMouse(int *, int *);
  // Handle arrow Keys with the current selection and the scancode
  void HandleKeys(SDL_Scancode *);
  // Init a new frame
  void ClearNewFrame();
  // Draw current frame
  void DrawCurrentFrame();
  // Draw an X
  void DrawX(TokenCross *);
  // Store GameMap
  void RoundTurn();
  void AnimateToken();
  int GetTokenDestination();
  // Sets an X at a given location and colour
  void SetToken(TokenCross *, int, TokenColour *, int);
  // Get current Selected field
  int *GetiSelection();
  bool DecreaseiSelection();
  bool IncreaseiSelection();
  // Get temporarly field
  int *GetiTempSelection();
  // Set temporarly field
  void SetiTempSelection(int *);
  // Get current TokenSelection
  TokenCross *GetTokenSelection();
  // See if the game was exited
  bool GetGameExStatus();
  // Exit the game
  void SetGameExStatus();
  // See if there is animation to be done
  bool GetAnimationStatus();
  // Search for a winner
  bool CheckifFour();
  // Animate winning situation
  void AnimateWin();

  /* Make the 4 winning tokens spinning anti clockwise
  In one tick each token's 4 points shift like:
  upper left point: P1(x,y) = x, y + 1
  lower right: P2(x,y) = x, y - 1
  upper right point: P3(x,y) = x - 1, y
  lower left pont: P4(x,y) = x + 1, y
  825 - 695 = 130 - offset*2 = 110 = borders for rotation
  After 110 frames, the rotation resets: This doesnt matter as long as the object is symmetrical in both ways, the eye cannot make it out and thinks it spins 360 deg
  An incrementing rotation step would have to look like this: pCrossTile->Cross[0] = {pCrossTile->Cross[0].x, pCrossTile->Cross[0].y + *RValue};
  */
  void RotateToken(TokenCross *, int, int *);
  // Function to quit
  void QuitGame();
  // Restart to a new game
  void RestartGame();
};
