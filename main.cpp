#include "fourWins.h"

int main(int argc, char *args[]) {
  // Init game
  cGame cGameFw;
  // Handle Input
  SDL_Event e;
  // Mouse Cords, only used for analyzing things
  int mx, my;

  // Flag to check if mouse is used
  bool bUsedMouse = false;

  while (cGameFw.GetGameExStatus() == false) {
    cGameFw.ClearNewFrame();

    // Handle Input if nothing is being animated
    while (SDL_PollEvent(&e)) {
      if (e.type == SDL_QUIT) {
        cGameFw.SetGameExStatus();
      }
      if (e.type == SDL_MOUSEMOTION) {
        //bUsedMouse = false; // Only used for debugging
      }
      if (e.type == SDL_KEYDOWN) {
        cGameFw.HandleKeys(&e.key.keysym.scancode);
      }
    }
    if (bUsedMouse) {
      SDL_GetMouseState(&mx, &my);
      std::cout << "x,y: " << mx << "," << my << std::endl;
    }
    bUsedMouse = false;
    
    cGameFw.DrawCurrentFrame();
  }
  cGameFw.QuitGame();
  return 0;
}
