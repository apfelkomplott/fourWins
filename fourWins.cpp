#include "fourWins.h"

cGame::cGame()
{
    // Make sure the right sdl version is being used to link and compile
    SDL_VERSION(&vcompiled);
    SDL_GetVersion(&vlinked);
    std::cout << "Compiled against SDL version: " << (int)vcompiled.major << "." << (int)vcompiled.minor << "." << (int)vcompiled.patch << std::endl;
    std::cout << "Linked against SDL version: " << (int)vlinked.major << "." << (int)vlinked.minor << "." << (int)vlinked.patch << std::endl;

    // Setup video
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0) {
        std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
        bEndGame = true;
        return;
    }

    // Create a window
    pWindow = SDL_CreateWindow("FourWins", SDL_WINDOWPOS_UNDEFINED,
                               SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH,
                               SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if (pWindow == NULL) {
        std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
        bEndGame = true;
        return;
    }

    // Create a renderer
    pRenderer = SDL_CreateRenderer(pWindow, -1, SDL_RENDERER_ACCELERATED);
    if (pRenderer == NULL) {
        std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
        bEndGame = true;
        return;
    }

    // Initialize audio
    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0) {
        std::cout << "Mix_OpenAudio Error: " << Mix_GetError() << std::endl;
        bEndGame = true;
        return;
    }
    // Load audio
    pSoundSelect = Mix_LoadWAV("select.wav");
    if (pSoundSelect == nullptr) {
        std::cout << "Failed to load select.wav" << Mix_GetError()
                  << std::endl;
        bEndGame = true;
        return;
    }
    pSoundSetToken = Mix_LoadWAV("set.wav");
    if (pSoundSelect == nullptr) {
        std::cout << "Failed to load set.wav" << Mix_GetError()
                  << std::endl;
        bEndGame = true;
        return;
    }
    pSoundWin = Mix_LoadWAV("win.wav");
    if (pSoundWin == nullptr) {
        std::cout << "Failed to load win.wav" << Mix_GetError()
                  << std::endl;
        bEndGame = true;
        return;
    }

    // Initialize font lib
    if (TTF_Init() != 0) {
        std::cout << "TTF_Init Error: " << TTF_GetError() << std::endl;
        bEndGame = true;
        return;
    }

    // Load fonts
    pfont = TTF_OpenFont("LiberationMono-Regular.ttf", 12);
    if (pfont == nullptr) {
        std::cout << "TTF_OpenFont Error: " << TTF_GetError() << std::endl;
        bEndGame = true;
        return;
    }

    pSurfaceText = TTF_RenderUTF8_Blended(pfont, sText.c_str(), cBlue);
    if (pSurfaceText == nullptr) {
        std::cout << "TTF_RenderUTF8_Solid Error: " << TTF_GetError() << std::endl;
        bEndGame = true;
        return;
    }

    pTexture = SDL_CreateTextureFromSurface(pRenderer, pSurfaceText);
    if (pTexture == nullptr) {
        std::cout << "SDL_CreateTextureFromSurface Error: " << SDL_GetError() << std::endl;
        bEndGame = true;
        return;
    }
    SDL_FreeSurface(pSurfaceText);
    pSurfaceText = nullptr;

    // Init the inner Grid and selection
    InitinnerGrid();
    TokenSelection.sTokenColour = GreenToken;
    SetToken(GetTokenSelection(), *GetiSelection(), &GetTokenSelection()->sTokenColour, 0);
}
void cGame::InitinnerGrid()
{
    for (int a = 0; a < rows; a++) {
        for (int i = 0; i < columns; i++) {
            innerGrid[i + (a * columns)].x = 45 + i * 130;
            innerGrid[i + (a * columns)].y = 45 + a * 130;
            innerGrid[i + (a * columns)].w = 130;
            innerGrid[i + (a * columns)].h = 130;
        }
    }
}
void cGame::ClearNewFrame()
{
    // Wait 33ms to archieve around 30 frames per second
    SDL_Delay(33);
    // Clear screen
    SDL_SetRenderDrawColor(pRenderer, 0, 0, 0, 255);
    SDL_RenderClear(pRenderer);
}
void cGame::DrawCurrentFrame()
{
    // Was a new column selected ?
    if (*GetiSelection() != *GetiTempSelection()) {
        SetToken(GetTokenSelection(), *GetiSelection(), &GetTokenSelection()->sTokenColour, 0);
        SetiTempSelection(GetiSelection());
    }

    DrawGrid();
    SDL_RenderCopy(pRenderer, pTexture, NULL, &rText);
    SDL_RenderPresent(pRenderer);
}
void cGame::DrawGrid()
{
    // Draw the outer Grid
    SDL_SetRenderDrawColor(pRenderer, cGrid.r, cGrid.g, cGrid.b, cGrid.a);
    SDL_RenderDrawRect(pRenderer, &outRect);

    // Draw the InnerGrid
    for (int i = 0; i < 42; i++) {
        SDL_RenderDrawRect(pRenderer, &innerGrid[i]);
    }
    // Draw selected square if nothing is being animated and none won
    if (!bAnimateSet && !bAnimateWin) {
        // std::cout << bAnimateSet << bAnimateWin;
        DrawX(&TokenSelection);
    }
    // Draw the setted X tokens
    for (int i = 0; i < 42; i++) {
        if (TokenMap[i].bSet == true) {
            DrawX(&TokenMap[i]);
        }
    }
    // Draw Animations
    if (cGame::bAnimateSet) {
        AnimateToken();
    }
    if (cGame::bAnimateWin) {
        AnimateWin();
    }
}
int cGame::GetMouse(int *mx, int *my)
{
    // A selections is only set if the mouse in in the field range of x[45;955] and
    // y[45;825]
    SDL_GetMouseState(mx, my);
    if (*mx <= 45 || *mx >= 955 || *my <= 45 || *my >= 825) {
        return -1;
    } else {
        int sqx = ((*mx - 45) / 130);
        int sqy = ((*my - 45) / 130);
        // 0 - 41
        return (sqx + (sqy * 7));
    }
}
void cGame::HandleKeys(SDL_Scancode *SKey)
{
    // If there is nothing animated or none has won, handle input, escape is always allowed
    if (*SKey == SDL_SCANCODE_ESCAPE) {
        bEndGame = true;
        return;
    }
    if (!bAnimateSet && !bAnimateWin) {
        switch (*SKey) {
        case SDL_SCANCODE_LEFT:
            if (iSelection > 0) {
                if (DecreaseiSelection()) {
                    Mix_PlayChannel(-1, pSoundSelect, 0);
                }
            }
            return;
        case SDL_SCANCODE_RIGHT:
            if (iSelection < 6) {
                if (IncreaseiSelection()) {
                    Mix_PlayChannel(-1, pSoundSelect, 0);
                }
            }
            return;
        case SDL_SCANCODE_SPACE:
            if (!TokenMap[iSelection].bSet) {
                iScheduler = GetTokenDestination();
                iAnimateSet = (innerGrid[iScheduler].y) - 45;
                iAnimateStart = innerGrid[iSelection].y;
                bAnimateSet = true;
            }
            return;
        default:
            std::cout << "unhandled key pressed: " << *SKey << std::endl;
        }
    }
    if (bAnimateWin && *SKey == SDL_SCANCODE_SPACE) {
        cGame::RestartGame();
        return;
    }
}
void cGame::SetToken(TokenCross *pCrossTile, int iTarget, TokenColour *Tcolor, int yValue)
{
    // Set an X with 2 Lines, X at rect 1 would be
    // xy1(45,45) to xy2(175,175) and xy3(175,45) to xy4(45,175)
    // From the current selection, go down until the bottom
    // yValue is used for the set animation

    pCrossTile->Cross[0] = {innerGrid[iTarget].x, innerGrid[iTarget].y + yValue};             // upper left crosspoint
    pCrossTile->Cross[1] = {innerGrid[iTarget].x + 130, innerGrid[iTarget].y + 130 + yValue}; // lower right crosspoint
    pCrossTile->Cross[2] = {innerGrid[iTarget].x + 130, innerGrid[iTarget].y + yValue};       // upper right crosspoint
    pCrossTile->Cross[3] = {innerGrid[iTarget].x, innerGrid[iTarget].y + 130 + yValue};       // lower left crosspoint
    pCrossTile->sTokenColour = *Tcolor;
}
int *cGame::GetiSelection()
{
    return &iSelection;
}
bool cGame::DecreaseiSelection()
{
    for (int i = iSelection - 1; i >= 0; i--) {
        if (!TokenMap[i].bSet) {
            iSelection = i;
            return true;
        }
    }
    return false;
}
bool cGame::IncreaseiSelection()
{
    for (int i = iSelection + 1; i <= 6; i++) {
        if (!TokenMap[i].bSet) {
            iSelection = i;
            return true;
        }
    }
    return false;
}
int *cGame::GetiTempSelection()
{
    return &iTempSelection;
}
void cGame::SetiTempSelection(int *iTarget)
{
    iTempSelection = *iTarget;
}
cGame::TokenCross *cGame::GetTokenSelection()
{
    TokenCross *pTokenSelection = &TokenSelection;
    return pTokenSelection;
}
bool cGame::GetGameExStatus()
{
    return bEndGame;
}
bool cGame::GetAnimationStatus()
{
    return bAnimateSet;
}
void cGame::SetGameExStatus()
{
    bEndGame = true;
}
void cGame::DrawX(TokenCross *CrossTile)
{
    SDL_SetRenderDrawColor(pRenderer, CrossTile->sTokenColour.cToken.r, CrossTile->sTokenColour.cToken.g, CrossTile->sTokenColour.cToken.b, CrossTile->sTokenColour.cToken.a);
    SDL_RenderDrawLine(pRenderer, CrossTile->Cross[0].x + iOffset, CrossTile->Cross[0].y + iOffset, CrossTile->Cross[1].x - iOffset, CrossTile->Cross[1].y - iOffset);
    SDL_RenderDrawLine(pRenderer, CrossTile->Cross[2].x - iOffset, CrossTile->Cross[2].y + iOffset, CrossTile->Cross[3].x + iOffset, CrossTile->Cross[3].y - iOffset);
}
int cGame::GetTokenDestination()
{
    int iTarget = -1;
    int icurrenCol = -1;
    int ifieldbelow = -1;
    // Get the column 41/7 = [0;6]
    icurrenCol = iSelection % columns;

    // Are there Tokens below?
    for (int i = 1; i < rows; i++) {
        ifieldbelow = icurrenCol + (i * columns);
        if (TokenMap[ifieldbelow].bSet) {
            std::cout << "Token below was found at: " << ifieldbelow << std::endl;
            iTarget = (ifieldbelow - columns);
            return iTarget;
        }
    }
    // Nothing below found
    return ifieldbelow;
}
void cGame::AnimateToken()
{
    // Move the Token from iAniamteStart to iAnimateSet
    if (iAnimateStart >= iAnimateSet) {
        bAnimateSet = false;
        SetToken(&TokenMap[iScheduler], iScheduler, &TokenSelection.sTokenColour, 0);
        TokenMap[iScheduler].bSet = true;
        // Is the column full ?
        if (iSelection == iScheduler) {
            if (!DecreaseiSelection()) {
                IncreaseiSelection();
            }
        }
        iScheduler = 0;
        RoundTurn();
        Mix_PlayChannel(-1, pSoundSetToken, 0);
        return;
    }
    SetToken(&TokenMap[iSelection], iSelection, &TokenSelection.sTokenColour, iAnimateStart);
    DrawX(&TokenMap[iSelection]);
    iAnimateStart += 30;
}
void cGame::RoundTurn()
{
    // Before the next round, we check if there is a winner
    if (RoundCount >= 4) {
        if (CheckifFour()) {
            std::cout << TokenSelection.sTokenColour.TokenString << " has won" << std::endl;
            bAnimateWin = true;
            Mix_PlayChannel(-1, pSoundWin, 0);
        }
    }
    // Green starts, every even Roundnumber green, every uneven Roundnumber blue
    if (RoundCount % 2 == 0) {
        TokenSelection.sTokenColour = GreenToken;
    } else {
        TokenSelection.sTokenColour = BlueToken;
    }
    RoundCount++;
    // Is the map full/ is there a draw?
    if (RoundCount == 43 && !bAnimateWin) {
        RestartGame();
    }
}
bool cGame::CheckifFour()
{
    // Init to current colour, only for the last setted token conditions are checked
    int iWinCount = 0;
    eToken eWinColour = TokenSelection.sTokenColour.TokenColourName;
    // Are there 4 of the same colour in a row ?
    for (int y = 0; y < 6; y++) {
        for (int i = 0; i < 7; i++) {
            if (TokenMap[i + (y * 7)].bSet && eWinColour == TokenMap[i + (y * 7)].sTokenColour.TokenColourName) {
                winfields[iWinCount] = i + (y * 7);
                iWinCount++;
            } else {
                winfields.fill(0);
                iWinCount = 0;
            }
            if (iWinCount == 4) {
                return 1;
            }
        }
        winfields.fill(0);
        iWinCount = 0;
    }
    // Are there 4 of the same colour in a coloumn ?
    for (int i = 0; i < 7; i++) {
        for (int y = 0; y < 6; y++) {
            if (TokenMap[i + (y * 7)].bSet && eWinColour == TokenMap[i + (y * 7)].sTokenColour.TokenColourName) {
                winfields[iWinCount] = i + (y * 7);
                iWinCount++;
            } else {
                winfields.fill(0);
                iWinCount = 0;
            }
            if (iWinCount == 4) {
                return 1;
            }
        }
        winfields.fill(0);
        iWinCount = 0;
    }
    // Are there 4 of the same colour diagonal ? Checking the upper left 3x4 box
    for (int y = 0; y < 3; y++) {
        for (int i = 0; i < 4; i++) {
            if (TokenMap[i + (y * 7)].bSet && eWinColour == TokenMap[i + (y * 7)].sTokenColour.TokenColourName) {
                winfields[iWinCount] = i + (y * 7);
                iWinCount = 1;
                for (int x = 1; x <= 4; x++) {
                    if (TokenMap[i + (y * 7) + (x * 7) + x].bSet && eWinColour == TokenMap[i + (y * 7) + (x * 7) + x].sTokenColour.TokenColourName) {
                        winfields[iWinCount] = i + (y * 7) + (x * 7) + x;
                        iWinCount++;
                        std::cout << "iWindCount: " << iWinCount << "at: " << (i + (y * 7) + (x * 7) + x) << std::endl;
                    } else {
                        winfields.fill(0);
                        iWinCount = 0;
                    }
                    if (iWinCount == 4) {
                        return 1;
                    }
                }
            } else {
                winfields.fill(0);
                iWinCount = 0;
            }
        }
    }
    // Are there 4 of the same colour diagonal ? Checking the upper right 3x4 box
    for (int y = 0; y < 3; y++) {
        for (int i = 3; i < 7; i++) {
            if (TokenMap[i + (y * 7)].bSet && eWinColour == TokenMap[i + (y * 7)].sTokenColour.TokenColourName) {
                winfields[iWinCount] = i + (y * 7);
                iWinCount = 1;
                for (int x = 1; x <= 4; x++) {
                    if (TokenMap[i + (y * 7) + (x * 7) - x].bSet && eWinColour == TokenMap[i + (y * 7) + (x * 7) - x].sTokenColour.TokenColourName) {
                        winfields[iWinCount] = i + (y * 7) + (x * 7) - x;
                        iWinCount++;
                        std::cout << "iWindCount: " << iWinCount << "at: " << (i + (y * 7) + (x * 7) - x) << std::endl;
                    } else {
                        winfields.fill(0);
                        iWinCount = 0;
                    }
                    if (iWinCount == 4) {
                        return 1;
                    }
                }
            } else {
                winfields.fill(0);
                iWinCount = 0;
            }
        }
    }
    return 0;
}
void cGame::AnimateWin()
{
    if (iRotationValue == 110) {
        iRotationValue = 0;
    }
    for (int i = 0; i < 4; i++) {
        RotateToken(&TokenMap[winfields[i]], winfields[i], &iRotationValue);
    }
    iRotationValue += 2;
}
void cGame::RotateToken(TokenCross *pCrossTile, int iTarget, int *RValue)
{
    pCrossTile->Cross[0] = {innerGrid[iTarget].x, innerGrid[iTarget].y + *RValue};             // upper left crosspoint
    pCrossTile->Cross[1] = {innerGrid[iTarget].x + 130, innerGrid[iTarget].y + 130 - *RValue}; // lower right crosspoint
    pCrossTile->Cross[2] = {innerGrid[iTarget].x + 130 - *RValue, innerGrid[iTarget].y};       // upper right crosspoint
    pCrossTile->Cross[3] = {innerGrid[iTarget].x + *RValue, innerGrid[iTarget].y + 130};       // lower left crosspoint
}
void cGame::QuitGame()
{
    // Clean up
    SDL_DestroyRenderer(pRenderer);
    SDL_DestroyWindow(pWindow);
    Mix_FreeChunk(pSoundSelect);
    Mix_FreeChunk(pSoundSetToken);
    Mix_FreeChunk(pSoundWin);
    TTF_CloseFont(pfont);
    pRenderer = nullptr;
    pWindow = nullptr;
    pSoundSelect = nullptr;
    pSoundSetToken = nullptr;
    pSoundWin = nullptr;
    pfont = nullptr;
    pTexture = nullptr;
    Mix_Quit();
    TTF_Quit();
    SDL_Quit();
    std::cout << "QuitGame executed" << std::endl;
}
void cGame::RestartGame()
{
    bAnimateWin = false;
    RoundCount = 1;
    TokenSelection.sTokenColour = GreenToken;
    iRotationValue = 0;
    for (int i = 0; i < 42; i++) {
        TokenMap[i].bSet = false;
    }
    std::cout << "Game restarted";
}